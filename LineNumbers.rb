#home = "#{ENV['HOMEDRIVE']}" + "#{ENV['HOMEPATH']}"
#filename = ARGV[0] || (home + '\\Lines.txt')
def get_lines(lines)
   File.open(lines, 'r').readlines
end
def get_format(lines)
   "%0#{lines.size.to_s.size}d"
end
def get_output(lines)
  format = get_format(lines)

output = ''
lines.each_with_index do |line, i|
  output += "#{sprintf(format,i+1)}: #{line}"
end
 output
end

#print get_output(get_line[Lines.txt])